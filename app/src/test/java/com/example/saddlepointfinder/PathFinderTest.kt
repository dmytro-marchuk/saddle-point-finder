package com.example.saddlepointfinder

import org.junit.Assert
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class PathFinderTest {
    private val finder = SaddlePointFinder()
    private fun doTest(
        sourceMatrix: Array<IntArray>,
        expectedValues: ArrayList<Point>
    ) {
        val result = finder.findSaddlePoints(sourceMatrix)
        Assert.assertEquals(expectedValues.size, result.size)
        result.forEach { Assert.assertTrue(expectedValues.contains(it)) }
    }

    @Test
    fun testCase1() {
        println("Test case #1")
        doTest(
            arrayOf(
                intArrayOf(8, 7, 0, 10),
                intArrayOf(6, 8, 5, 9),
                intArrayOf(6, 8, 5, 10)
            ), arrayListOf(
                Point(5, 1, 2),
                Point(9, 1, 3),
                Point(5, 2, 2)
            )
        )
    }

    @Test
    fun testCase2() {
        println("Test case #2")
        doTest(
            arrayOf(
                intArrayOf(1, 2),
                intArrayOf(3, 4)
            ), arrayListOf(
                Point(2, 0, 1),
                Point(3, 1, 0)
            )
        )
    }

    @Test
    fun testCase3() {
        println("Test case #3")
        doTest(
            arrayOf(
                intArrayOf(4, 5, 9, 3),
                intArrayOf(8, 4, 3, 7),
                intArrayOf(7, 6, 8, 9),
                intArrayOf(7, 2, 4, 6)
            ), arrayListOf(
                Point(6, 2, 1)
            )
        )
    }
}
