package com.example.saddlepointfinder

import java.util.*

class Point(private val value: Int, private val row: Int, private val column: Int) {
    private var isMinInRow: Boolean = false
    private var isMaxInRow: Boolean = false
    private var isMinInCol: Boolean = false
    private var isMaxInCol: Boolean = false

    fun getValue() = value
    fun isSaddlePoint() = isMinInRow && isMaxInCol || isMaxInRow && isMinInCol

    fun markAsMinInRow() {
        isMinInRow = true
    }

    fun markAsMaxInRow() {
        isMaxInRow = true
    }

    fun markAsMinInColumn() {
        isMinInCol = true
    }

    fun markAsMaxInColumn() {
        isMaxInCol = true
    }

    override fun toString(): String {
        return String.format(Locale.getDefault(), "[%d, %d], %d", row, column, value)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (other == null) {
            return false
        }
        if (!(other is Point)) {
            return false
        }
        return (this.row == other.row) && (this.column == other.column) && (this.value == other.value)
    }

    override fun hashCode(): Int {
        var result = value
        result = 31 * result + row
        result = 31 * result + column
        result = 31 * result + isMinInRow.hashCode()
        result = 31 * result + isMaxInRow.hashCode()
        result = 31 * result + isMinInCol.hashCode()
        result = 31 * result + isMaxInCol.hashCode()
        return result
    }
}