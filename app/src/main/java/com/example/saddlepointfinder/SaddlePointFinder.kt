package com.example.saddlepointfinder

import java.util.*

class SaddlePointFinder {
    fun findSaddlePoints(source: Array<IntArray>): List<Point> {
        val m = source.size
        val n = source[0].size
        val matrix = createMatrixOfPoint(m, n, source)
        for (value in matrix) {
            var min = value[0]
            var max = value[0]
            for (j in 1 until n) {
                val currentItem = value[j]
                if (currentItem.getValue() == min.getValue()) {
                    currentItem.markAsMinInRow()
                }
                if (currentItem.getValue() == max.getValue()) {
                    currentItem.markAsMaxInRow()
                }
                if (currentItem.getValue() < min.getValue()) {
                    min = currentItem
                } else if (currentItem.getValue() > max.getValue()) {
                    max = currentItem
                }
            }
            min.markAsMinInRow()
            max.markAsMaxInRow()
        }
        for (i in 0 until n) {
            var min = matrix[0][i]
            var max = matrix[0][i]

            for (j in 1 until m) {
                val currentItem = matrix[j][i]
                if (currentItem.getValue() == min.getValue()) {
                    currentItem.markAsMinInColumn()
                }
                if (currentItem.getValue() == max.getValue()) {
                    currentItem.markAsMaxInColumn()
                }
                if (currentItem.getValue() < min.getValue()) {
                    min = currentItem
                } else if (currentItem.getValue() > max.getValue()) {
                    max = currentItem
                }
            }
            min.markAsMinInColumn()
            max.markAsMaxInColumn()
        }

        val saddlePoints = ArrayList<Point>()
        for (points in matrix) {
            for (j in 0 until n) {
                if (points[j].isSaddlePoint()) {
                    println("Found a saddle point: " + points[j])
                    saddlePoints.add(points[j])
                }
            }
        }
        return saddlePoints
    }

    private fun createMatrixOfPoint(m: Int, n: Int, source: Array<IntArray>): Array<Array<Point>> {
        val matrix = Array<Array<Point>>(m) { arrayOf() }
        for (i in 0 until m) {
            val row: Array<Point?> = arrayOfNulls(n)
            for (j in 0 until n) {
                row[j] = Point(source[i][j], i, j)
            }
            matrix[i] = row.requireNoNulls()
        }
        return matrix
    }
}
